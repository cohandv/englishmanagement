package com.english.Utils;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.widget.ImageView;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.StringTokenizer;

/**
 * Created by David on 30/8/2017.
 */

public class DownloadImageHelper extends AsyncTask<ListItemRequest, Void, Bitmap> {

    private Exception exception;
    private ListItemRequest delegate;


    private InputStream OpenHttpConnection(String urlString) throws IOException {
        InputStream in = null;
        int response = -1;

        URL url = new URL(urlString);
        URLConnection conn = url.openConnection();

        if (!(conn instanceof HttpURLConnection))
            throw new IOException("Not an HTTP connection");

        try {
            HttpURLConnection httpConn = (HttpURLConnection) conn;
            httpConn.setAllowUserInteraction(false);
            httpConn.setInstanceFollowRedirects(true);
            httpConn.setRequestMethod("GET");
            httpConn.connect();
            response = httpConn.getResponseCode();
            if (response == HttpURLConnection.HTTP_OK) {
                in = httpConn.getInputStream();
            }
        } catch (Exception ex) {
            throw new IOException("Error connecting");
        }
        return in;
    }

    @Override
    protected Bitmap doInBackground(ListItemRequest... req) {
        Bitmap bitmap = null;
        InputStream in = null;
        this.delegate = req[0];
        try {
            in = OpenHttpConnection(this.delegate.getUrl());
            bitmap = BitmapFactory.decodeStream(in);
            in.close();
        } catch (IOException e) {
            this.exception = e;
            e.printStackTrace();
            return null;
        }
        return bitmap;
    }

    @Override
    protected void onPostExecute(Bitmap result) {

        this.delegate.getDelegate().bindImage(this.delegate.getPosition(), result);
    }
}
