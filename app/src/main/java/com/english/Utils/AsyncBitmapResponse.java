package com.english.Utils;

import android.graphics.Bitmap;

/**
 * Created by David on 30/8/2017.
 */

public interface AsyncBitmapResponse {
    void bindImage(Integer position, Bitmap bitmap);
}
