package com.english.Utils;

/**
 * Created by David on 30/8/2017.
 */

public class ListItemRequest {
    private Integer position;
    private String url;
    private AsyncBitmapResponse delegate;

    public ListItemRequest(Integer position, String url, AsyncBitmapResponse delegate) {
        this.setPosition(position);
        this.setUrl(url);
        this.setDelegate(delegate);
    }

    public Integer getPosition() {
        return position;
    }

    public void setPosition(Integer position) {
        this.position = position;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url= url;
    }

    public AsyncBitmapResponse getDelegate() {
        return delegate;
    }

    public void setDelegate(AsyncBitmapResponse delegate ) {
        this.delegate = delegate;
    }

}
