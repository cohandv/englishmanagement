package com.english.models;

public class Feed {

    private int id;
    private String label;
    private String content;
    private String icon;

    public Feed(int id, String content, String label, String icon) {
        this.id = id;
        this.content = content;
        this.label = label;
        this.icon = icon;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getContent() {
        return content;
    }

    public String getLabel() {
        return label;
    }

    public void setContent(String content) {
        this.content = content;
    }
    public void setLabel(String label) {
        this.label = label;
    }

    public String getIcon() {
        return icon;
    }
    public void setIcon(String icon) { this.icon = icon; }
}
