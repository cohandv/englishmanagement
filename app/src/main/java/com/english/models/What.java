package com.english.models;

import java.io.InputStream;
import java.util.ArrayList;

public class What {

    private InputStream img;
    private String label;
    private ArrayList<Programs> programs;

    public What(InputStream img, String label, ArrayList<Programs> programs) {
        this.img = img;
        this.label = label;
        this.programs = programs;
    }

    public InputStream getImg() {
        return img;
    }

    public void setImg(InputStream img) {
        this.img = img;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public ArrayList<Programs> getPrograms() {
        return programs;
    }

    public void setPrograms(ArrayList<Programs> programs) {
        this.programs = programs;
    }
}
