package com.english.models;

import java.io.InputStream;

public class Who {

    private int id;
    private String name;
    private Integer age;
    private String sex;
    private String role;
    private String courses;
    private String funny;
    private InputStream icon;

    public Who(int id, String name, Integer age, String sex, String role, String courses, String funny, InputStream icon) {
        this.id = id;
        this.name = name;
        this.age = age;
        this.sex = sex;
        this.role = role;
        this.courses = courses;
        this.funny = funny;
        this.icon = icon;
    }

    public int getId() {
        return id;
    }
    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }

    public Integer getAge() {
        return age;
    }
    public void setAge(Integer age) {
        this.age = age;
    }

    public String getSex() {
        return sex;
    }
    public void setSex(String sex) {
        this.sex = sex;
    }

    public String getRole() {
        return role;
    }
    public void setRole(String role) {
        this.role= role;
    }

    public String getCourses() {
        return courses;
    }
    public void setCourses(String courses) {
        this.courses = courses;
    }

    public String getFunny() {
        return funny;
    }
    public void setFunny(String funny) {
        this.funny= funny;
    }

    public InputStream getIcon() {
        return icon;
    }
    public void setIcon(InputStream icon) { this.icon = icon; }
}
