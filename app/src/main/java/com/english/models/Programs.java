package com.english.models;

public class Programs {

    private int id;
    private String level;
    private String label;

    public Programs(int id, String level, String label) {
        this.id = id;
        this.level = level;
        this.label = label;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getLevel() {
        return level;
    }

    public void setLevel(String level) {
        this.level = level;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }
}
