package com.english.models;

public class Calendar {

    private int id;
    private String day;
    private String label;
    private String teacher;

    public Calendar(int id, String day, String label, String teacher) {
        this.id = id;
        this.day = day;
        this.label = label;
        this.teacher = teacher;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDay() {
        return day;
    }

    public void setDay(String day) {
        this.day = day;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public String getTeacher() {
        return teacher;
    }

    public void setTeacher(String teacher) {
        this.teacher = teacher;
    }
}
