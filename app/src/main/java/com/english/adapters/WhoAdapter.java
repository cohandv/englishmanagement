package com.english.adapters;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.english.R;
import com.english.Utils.AsyncBitmapResponse;
import com.english.Utils.DownloadImageHelper;
import com.english.Utils.ListItemRequest;
import com.english.models.Who;

import org.w3c.dom.Text;

import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Array;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;

public class WhoAdapter extends RecyclerView.Adapter<WhoAdapter.ViewHolder> implements AsyncBitmapResponse {
    private ArrayList<Who> items;
    private ArrayList<ImageView> icons;


    public WhoAdapter(ArrayList<Who> items) {

        this.items = items;
        this.icons = new ArrayList<ImageView>();
    }

    @Override
    public void bindImage(Integer position, Bitmap bitmap) {
        this.icons.get(position).setImageBitmap(bitmap);
    }


    @Override
    public WhoAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_who, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(WhoAdapter.ViewHolder holder, int position) {
        holder.name.setText(items.get(position).getName());
        holder.age.setText(Html.fromHtml("<b>Age: </b> " + items.get(position).getAge()));
        holder.sex.setText(Html.fromHtml("<b>Gender: </b> " + items.get(position).getSex()));
        holder.role.setText(Html.fromHtml("<b>Role: </b> " + items.get(position).getRole()));
        holder.courses.setText(Html.fromHtml("<b>Courses: </b> " + items.get(position).getCourses()));
        holder.funny.setText(Html.fromHtml("<b>Facts: </b> " + items.get(position).getFunny()));

        Drawable d = Drawable.createFromStream(items.get(position).getIcon(), null);
        holder.icon.setImageDrawable(d);
        //"<b>" + id + "</b> " + name;
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    static class ViewHolder extends RecyclerView.ViewHolder {
        TextView name;
        TextView age;
        TextView sex;
        TextView role;
        TextView courses;
        TextView funny;
        ImageView icon;

        public ViewHolder(View itemView) {
            super(itemView);
            name = (TextView) itemView.findViewById(R.id.name);
            age = (TextView) itemView.findViewById(R.id.age);
            sex = (TextView) itemView.findViewById(R.id.sex);
            role = (TextView) itemView.findViewById(R.id.role);
            courses = (TextView) itemView.findViewById(R.id.courses);
            funny = (TextView) itemView.findViewById(R.id.funny);
            icon = (ImageView) itemView.findViewById(R.id.icon);

        }
    }
}
