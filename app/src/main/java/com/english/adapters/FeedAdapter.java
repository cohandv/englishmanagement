package com.english.adapters;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.english.R;
import com.english.Utils.AsyncBitmapResponse;
import com.english.Utils.DownloadImageHelper;
import com.english.Utils.ListItemRequest;
import com.english.models.Feed;

import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Array;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;

public class FeedAdapter extends RecyclerView.Adapter<FeedAdapter.ViewHolder> implements AsyncBitmapResponse {
    private ArrayList<Feed> items;
    private ArrayList<ImageView> icons;


    public FeedAdapter(ArrayList<Feed> items) {

        this.items = items;
        this.icons = new ArrayList<ImageView>();
    }

    @Override
    public void bindImage(Integer position, Bitmap bitmap) {
        this.icons.get(position).setImageBitmap(bitmap);
    }


    @Override
    public FeedAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_feed, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(FeedAdapter.ViewHolder holder, int position) {
        holder.content.setText(items.get(position).getContent());
        holder.label.setText(items.get(position).getLabel());

        String uri = Uri.parse(items.get(position).getIcon()).buildUpon().
               build().toString();

        DownloadImageHelper downloadManager = new DownloadImageHelper();
        ListItemRequest lir = new ListItemRequest(position, uri, this);

        this.icons.add(holder.icon); //this cant be done worstly!
        downloadManager.execute(lir);

    }

    @Override
    public int getItemCount() {
        return items.size();
    }



    static class ViewHolder extends RecyclerView.ViewHolder {
        TextView content;
        TextView label;
        ImageView icon;

        public ViewHolder(View itemView) {
            super(itemView);
            content = (TextView) itemView.findViewById(R.id.content);
            label = (TextView) itemView.findViewById(R.id.label);
            icon = (ImageView) itemView.findViewById(R.id.icon);

        }
    }
}
