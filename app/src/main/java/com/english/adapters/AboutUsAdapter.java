package com.english.adapters;

import android.graphics.drawable.Drawable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.english.R;
import com.english.models.What;
import com.english.models.Who;

import java.util.ArrayList;

public class AboutUsAdapter extends RecyclerView.Adapter<AboutUsAdapter.ViewHolder> {
    private ArrayList<What> items;


    public AboutUsAdapter(ArrayList<What> items) {

        this.items = items;
    }


    @Override
    public AboutUsAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_us, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(AboutUsAdapter.ViewHolder holder, int position) {
        holder.label.setText(items.get(position).getLabel());
        Drawable d = Drawable.createFromStream(items.get(position).getImg(), null);
        holder.icon.setImageDrawable(d);
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    static class ViewHolder extends RecyclerView.ViewHolder {
        TextView label;
        ImageView icon;

        public ViewHolder(View itemView) {
            super(itemView);
            label = (TextView) itemView.findViewById(R.id.label);
            icon = (ImageView) itemView.findViewById(R.id.icon);
        }
    }
}
