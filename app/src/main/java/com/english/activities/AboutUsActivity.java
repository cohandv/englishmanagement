package com.english.activities;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.english.R;
import com.english.adapters.AboutUsAdapter;
import com.english.adapters.WhoAdapter;
import com.english.models.What;
import com.english.models.Who;

import org.json.JSONArray;
import org.json.JSONException;

import java.io.IOException;
import java.util.ArrayList;

public class AboutUsActivity extends AppCompatActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        setTitle("About Us");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_what);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        RecyclerView rv = (RecyclerView) findViewById(R.id.what);
        rv.setLayoutManager(new LinearLayoutManager(this));
        ArrayList<What> w = new ArrayList<>();
        try {
            w.add(new What(getAssets().open("ic_launcher.png"),"Ten years ago, the Institute had two different places. First place was in Jonte y San Nicolas and Second place was in J.V.Gonzalez and Lascano. Nowadays is in Emilio Lamarca and Arregui. \n" +
                    "In the beginning, it had six courses and nowadays it has ten courses. The Institute started with two teachers and now there are three teachers. There are 150 students up today. \n" +
                    "She was working at another institute, which name was  \"The way\", in this one, the teachers worked together and not against, like others. So when she started her own institute, she called it \"Your own way\", and it remained until now. She didn't like the idea of an English culture, so she called it language center (almost the same).\n" +
                    "Why should people choose this institute to study?\n" +
                    "The way to work. The dynamic. She thinks students must enjoy learning and teachers enjoy teaching, and that it the place is a comfortable place, the child will learn correctly.\n", null));
        } catch (IOException e) {
            e.printStackTrace();
        }
        AboutUsAdapter adapter = new AboutUsAdapter(w);
        rv.setAdapter(adapter);
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
}
