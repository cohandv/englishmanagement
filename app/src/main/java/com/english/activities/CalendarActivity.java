package com.english.activities;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.ColorFilter;
import android.graphics.PixelFormat;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.IntRange;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.english.R;
import com.english.adapters.WhoAdapter;
import com.english.models.Who;
import com.roomorama.caldroid.CaldroidFragment;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

public class CalendarActivity extends AppCompatActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        setTitle("Your Calendar");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_calendar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        CaldroidFragment caldroidFragment = new CaldroidFragment();
        Bundle args = new Bundle();
        Date currentTime = Calendar.getInstance().getTime();
        Calendar cal = Calendar.getInstance();
        args.putInt(CaldroidFragment.MONTH, cal.get(Calendar.MONTH) + 1);
        args.putInt(CaldroidFragment.YEAR, cal.get(Calendar.YEAR));
        caldroidFragment.setArguments(args);

        ColorDrawable green = new ColorDrawable(Color.GREEN);

        Calendar beginTime = Calendar.getInstance();
        beginTime.set(2017, 3, 6, 1, 1); //start date march 6 of 2017
        Calendar endTime = Calendar.getInstance();
        endTime.set(2019, 12, 31, 1, 1); //finish at the end of 2019

        while (beginTime.getTimeInMillis() < endTime.getTimeInMillis()) {

            int currMonth = beginTime.get(Calendar.MONTH);
            if (currMonth != Calendar.JANUARY && currMonth != Calendar.FEBRUARY) {
                caldroidFragment.setBackgroundDrawableForDate(green, beginTime.getTime());
            }
            beginTime.add(Calendar.DATE, 7);
        }

        beginTime = Calendar.getInstance();
        beginTime.set(2017, 3, 4, 1, 1); //start date march 6 of 2017
        endTime.set(2019, 12, 31, 1, 1); //finish at the end of 2019

        while (beginTime.getTimeInMillis() < endTime.getTimeInMillis()) {

            int currMonth = beginTime.get(Calendar.MONTH);
            if (currMonth != Calendar.JANUARY && currMonth != Calendar.FEBRUARY) {
                caldroidFragment.setBackgroundDrawableForDate(green, beginTime.getTime());
            }
            beginTime.add(Calendar.DATE, 7);
        }

        ColorDrawable blue = new ColorDrawable(Color.BLUE);
        beginTime = Calendar.getInstance();
        beginTime.set(2017, 4, 1, 1, 1); //start date march 6 of 2017
        endTime.set(2019, 12, 31, 1, 1); //finish at the end of 2019

        while (beginTime.getTimeInMillis() < endTime.getTimeInMillis()) {

            int currMonth = beginTime.get(Calendar.MONTH);
            if (currMonth != Calendar.JANUARY && currMonth != Calendar.FEBRUARY) {
                caldroidFragment.setBackgroundDrawableForDate(blue, beginTime.getTime());
            }
            beginTime.add(Calendar.DATE, 7);
        }

        ColorDrawable gray = new ColorDrawable(Color.GRAY);
        beginTime = Calendar.getInstance();
        beginTime.set(2016, 1, 6, 1, 1); //start date march 6 of 2017
        endTime.set(2020, 12, 31, 1, 1); //finish at the end of 2019

        while (beginTime.getTimeInMillis() < endTime.getTimeInMillis()) {
            caldroidFragment.setBackgroundDrawableForDate(gray, beginTime.getTime());
            beginTime.add(Calendar.DATE, 1);
            caldroidFragment.setBackgroundDrawableForDate(gray, beginTime.getTime());
            beginTime.add(Calendar.DATE, 6);
        }



        android.support.v4.app.FragmentTransaction t = getSupportFragmentManager().beginTransaction();
        t.replace(R.id.calendarView, caldroidFragment);
        t.commit();

    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
}
