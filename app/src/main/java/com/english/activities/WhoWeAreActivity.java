package com.english.activities;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.english.R;
import com.english.adapters.WhoAdapter;
import com.english.models.Who;

import org.json.JSONArray;
import org.json.JSONException;

import java.io.IOException;
import java.util.ArrayList;

public class WhoWeAreActivity extends AppCompatActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        setTitle("The Crew");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_who);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        RecyclerView rv = (RecyclerView) findViewById(R.id.who);
        rv.setLayoutManager(new LinearLayoutManager(this));
        ArrayList<Who> w = new ArrayList<>();
        try {
            w.add(new Who(1,
                    "Andrea Scioscia",
                    58,
                    "Female",
                    "She is the headmaster and have been working here for ten years.She teaches intensive children teens adults conversation and elementary",
                    "She is an english teacher, social psychologist, master in PNN, master in inclusive education.",
                    "She always starts her classes with the phrase, Letttsssss seee",
                    getAssets().open("andreasciocia.jpg")));

            w.add(new Who(1,
                    "El Maxi",
                    42,
                    "Male",
                    "He has been working here as mainteinance guy and secretary for the last 10 years.",
                    "He worked on several industries, lot of experience on selling.",
                    "his line is always: EZEQUIEEEEEEL!!",
                    getAssets().open("maxi.jpg")));

            w.add(new Who(1,
                    "Nacho  Villavicencio",
                    21,
                    "Male",
                    "He has been working since 2015 with 6th adults and intensive.",
                    "He is studying to become an english teacher.",
                    "He has a TOC with the order.",
                    getAssets().open("nacho.jpg")));

            w.add(new Who(1,
                    "Mica 'La Loca' Crosta",
                    22,
                    "Female",
                    "She has been working here for 3 years with children and teens groups, adults, intensive.",
                    "She is studying to become an english teacher.",
                    "When she doesnt shout, she is funny",
                    getAssets().open("mica.jpg")));

            w.add(new Who(1,
                    "Rosario 'somebody seen her?' Caira",
                    24,
                    "Female",
                    "She has been working here for 6 years with children and teens groups.",
                    "She is on the last year of odontology.",
                    "She is always smiling",
                    getAssets().open("rosario.jpg")));

        } catch (IOException e) {
            e.printStackTrace();
        }
        WhoAdapter adapter = new WhoAdapter(w);
        rv.setAdapter(adapter);

    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
}
