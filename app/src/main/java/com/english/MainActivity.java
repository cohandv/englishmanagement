package com.english;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;

import com.english.activities.AboutUsActivity;
import com.english.activities.CalendarActivity;
import com.english.activities.WhoWeAreActivity;
import com.english.adapters.FeedAdapter;
import com.english.models.Feed;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setTitle("Feeds");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        RecyclerView rv = (RecyclerView) findViewById(R.id.feed);
        rv.setLayoutManager(new LinearLayoutManager(this));
        ArrayList<Feed> f = new ArrayList<>();
        f.add(new Feed(1, "Today's class is suspended due to public transport strike.", "NEWS","http://peaksloanforgiveness.com/wp-content/uploads/2017/08/learn_1.jpg"));
        f.add(new Feed(1, "Next wednesday you have the 1st parcial, included from unit 1 to 3.", "NOTICE", "https://tctc.axstudent.com/images/courses_catalog/10"));
        f.add(new Feed(1, "1st of December is Mica's birthday!.", "BIRTHDAY'S","http://www.wishesgreeting.com/wp-content/uploads/2015/07/happy-birthday-wishes04.png"));
        f.add(new Feed(1, "Final test dates are available, check it out on the calendar.", "NEWS", "https://c.tadst.com/gfx/1200x630/calendar.png?1"));
        f.add(new Feed(1, "The end of year is comming and remember to read the two books for the final.", "NOTICE", "https://www.firefighterclosecalls.com/wp-content/uploads/2017/05/niEkRL6iA.png"));
        FeedAdapter adapter = new FeedAdapter(f);
        rv.setAdapter(adapter);
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }


    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {

        int id = item.getItemId();

        if (id == R.id.nav_calendar) {
            startActivity(new Intent(this, CalendarActivity.class));
        } else if (id == R.id.nav_company) {
            startActivity(new Intent(this, AboutUsActivity.class));
        } else if (id == R.id.nav_who) {
            startActivity(new Intent(this, WhoWeAreActivity.class));

        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
}
